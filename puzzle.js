var puzzle = (function(){
    var pieceListConst = [
                        'cell-0-0','cell-0-1','cell-0-2','cell-0-3',
                        'cell-1-0','cell-1-1','cell-1-2','cell-1-3',
                        'cell-2-0','cell-2-1','cell-2-2','cell-2-3',
                        'cell-3-0','cell-3-1','cell-3-2', 'cell-empty'
                        ];
    var pieceList = [
                        'cell-0-0','cell-0-1','cell-0-2','cell-0-3',
                        'cell-1-0','cell-1-1','cell-1-2','cell-1-3',
                        'cell-2-0','cell-2-1','cell-2-2','cell-2-3',
                        'cell-3-0','cell-3-1','cell-3-2', 'cell-empty'
                        ];
    var shuffle = function(a, dim) {
        var swap = function(x, y) {
            var temp = a[x];
            a[x] = a[y];
            a[y] = temp; 
        };
        var index = pieceList.length - 1;
        for (var i = 0; i < 500; i++) {
            var d = Math.floor(Math.random() * 12) % 4;
            if (d === 0) {
                if (index % dim - 1 >= 0) {
                    swap(index, index - 1);
                    index = index - 1;
                }
            } else if (d === 1) {
                if (index % dim + 1 < dim) {
                    swap(index, index + 1);
                    index = index + 1;
                }
            } else if (d === 2) {
                if (index - 4 >= 0) {
                    swap(index, index - 4);
                    index = index - 4;
                }
            } else {
                if (index + 4 < pieceList.length) {
                    swap(index, index + 4);
                    index = index + 4;
                }
            }
        }
    };

    var puzzle = {
        dim: 0,
        emptyCellLocation: [],
        pieces2d: [],
        stepCount: 0,
        updateStepCount: function(arg) {
            if (arg === 0) this.stepCount = arg;
            else this.stepCount++;
            document.getElementById('step-count').innerHTML = this.stepCount;
        },
        congratulate : function() {
            alert("You've completed the puzzle!");
        },

        movePiece: function(event) {
            e = event || window.event;
            var target = e.target || e.srcElement;
            if (target.nodeType == 3) target = target.parentNode; // safari
            this.t = target;
            
            var movable = false;
            for (var i = 0; i < this.dim; i++) {
                for (var j = 0; j < this.dim; j++) {
                    if (target == this.pieces2d[i][j]) {
                        // console.log(this.emptyCellLocation[0] +',' + this.emptyCellLocation[1]);
                        var row = i;
                        var col = j;
                        if (row == this.emptyCellLocation[0] 
                            && col == this.emptyCellLocation[1]) {
                            // clicking on the empty cell, do nothing
                            return;
                        }
                        else if ((row == this.emptyCellLocation[0] 
                                    && Math.abs(col - this.emptyCellLocation[1]) == 1)
                                || (col == this.emptyCellLocation[1] 
                                    && Math.abs(row - this.emptyCellLocation[0]) == 1)) {
                            movable = true;
                            for (var k = 0; k < target.classList.length; k++) {
                                var ele = target.classList[k];
                                if (ele != 'puzzle-cell' && ele != 'cell-movable' && ele != 'cell-unmovable') {
                                    target.classList.remove(ele);
                                    this.pieces2d[this.emptyCellLocation[0]][this.emptyCellLocation[1]].classList.remove('cell-empty');
                                    this.pieces2d[this.emptyCellLocation[0]][this.emptyCellLocation[1]].classList.add(ele);                                    
                                }
                            }
                            target.classList.add('cell-empty');
                            // update the empty cell location
                            this.emptyCellLocation = [row, col];
                        }
                    }
                }
            }
            if (!movable) {
                target.classList.add('cell-unmovable');
                window.setTimeout(function() {target.classList.remove('cell-unmovable')}, 70);
            }
            else {
                target.classList.add('cell-movable');
                window.setTimeout(function() {target.classList.remove('cell-movable')}, 70);
                // update step count
                this.updateStepCount();
            }

            if (this.checkComplete()) {
                window.setTimeout(this.congratulate, 100);;
            }
        },

        checkComplete: function() {
            if (!this.pieces2d[this.dim - 1][this.dim - 1].classList.contains('cell-empty')) {
                return false;
            }

            for (i = 0; i < this.dim; i++) {
                for (j = 0; j < this.dim; j++) {
                    if (i === this.dim - 1 && j === this.dim - 1) break;
                    if (!this.pieces2d[i][j].classList.contains('cell-' + i + '-' + j)) {
                        return false;
                    }
                }
            }

            // show the last piece when completed
            this.pieces2d[this.dim - 1][this.dim - 1].classList.remove('cell-empty');
            this.pieces2d[this.dim - 1][this.dim - 1].classList.add('cell-3-3');
            this.emptyCellLocation = [];

            return true;
        },

        createPuzzle: function(dimension) {
            this.dim = dimension;
            this.pieces2d = [];
            shuffle(pieceList, dimension);
            this.updateStepCount(0);
            // console.log(pieceList);
            var panel = document.getElementsByClassName('puzzle-panel')[0]
            panel.innerHTML = "";
            var pieceCount = 0
            for (var i = 0; i < this.dim; i++) {
                // create row
                var rowDiv = document.createElement('div');
                rowDiv.classList.add('puzzle-row');
                this.pieces2d[i] = [];
                // create cells in this row
                for (var j = 0; j < this.dim; j++) {
                    var cell = document.createElement('div');
                    cell.classList.add('puzzle-cell', pieceList[pieceCount++]);
                    if (cell.classList.contains('cell-empty')) {
                        this.emptyCellLocation = [i, j];
                    }
                    cell.onclick=("click", this.movePiece.bind(this));
                    rowDiv.appendChild(cell);
                    this.pieces2d[i][j] = cell;
                }
                // append row to panel
                panel.appendChild(rowDiv);
            }
        },
    };

    return puzzle;
})();

